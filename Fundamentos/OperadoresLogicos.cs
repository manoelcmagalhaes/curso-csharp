﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Fundamentos
{
    internal class OperadoresLogicos
    {
        public static void Executar()
        {
            var exeTrab1 = true;
            var exeTrab2 = false;
            var compraTv50 = exeTrab1 && exeTrab2;
            Console.WriteLine($"Comprou a tv 50? {compraTv50}");
            var compraSorv = exeTrab1 || exeTrab2;
            Console.WriteLine($"Comprou sorvete? {compraSorv}");
            var compraTv32 = exeTrab1 ^ exeTrab2;
            Console.WriteLine($"Comprou a tv 32? {compraTv32}");
            Console.WriteLine($"Está saudável? {!compraSorv}");
        }
    }
}
