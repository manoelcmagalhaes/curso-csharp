﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Fundamentos
{
    internal class LendoDados
    {
        public static void Executar()
        {
            Console.WriteLine("Qual seu nome?");
            var nome = Console.ReadLine();
            Console.WriteLine("Qual é a sua idade?");
            int idade = 0;
            try
            {
                idade = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine($"Você inseriu um valor incompativel: {e.Message}");
                while (idade == 0)
                {
                    Console.WriteLine("Qual é a sua idade?");
                    try
                    {
                        idade = int.Parse(Console.ReadLine());
                    }
                    catch (Exception e2)
                    {
                        Console.WriteLine("Tente novamente!");
                    }
                }
            }
            Console.WriteLine("Qual é o seu salario?");
            var salario = double.Parse(Console.ReadLine(), System.Globalization.CultureInfo.InvariantCulture);
            Console.WriteLine($"{nome} tem {idade} anos e recebe {salario} de salario.");
        }
    }
}
