﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Fundamentos
{
    internal class OperadoresUnarios
    {
        public static void Executar()
        {
            var valNega = -5;
            var numa1 = 2;
            var numa2 = 3;
            var bula = true;
            Console.WriteLine(-valNega);
            Console.WriteLine(!bula);
            numa1++;
            Console.WriteLine(numa1);
            --numa1;
            Console.WriteLine(numa1);
            Console.WriteLine(numa1++ == --numa2);
        }
    }
}
