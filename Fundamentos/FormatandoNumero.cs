﻿using System;
using System.Globalization;

namespace CursoCSharp.Fundamentos
{
    internal class FormatandoNumero
    {
        public static void Executar()
        {
            var valor = 15.175;
            //Float 1, define apenas uma casa decimal
            Console.WriteLine(valor.ToString("F1"));
            Console.WriteLine(valor.ToString("F2"));
            //C, de currency, Converte pra valor monetario do sistema
            Console.WriteLine(valor.ToString("C"));
            //Definição da cultura
            CultureInfo cultura = new CultureInfo("en-US");
            Console.WriteLine(valor.ToString("C3"), cultura);
            //P, de percentual, transforma o valor em percentual
            Console.WriteLine(valor.ToString("P"));
            //Define a quantidade de casas decimais
            Console.WriteLine(valor.ToString("#.##"));
            //Zeros a esquerda
            var inteiro = 256;
            Console.WriteLine(inteiro.ToString("D10"));
        }
    }
}
