﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Fundamentos
{
    internal class OperadoresAritimeticos
    {
        public static void Executar()
        {
            //Desconto
            var preco = 1000.0;
            var imposto = 355;
            var desconto = 0.1;

            double total = preco + imposto;
            var comDesconto = total - total * desconto;
            Console.WriteLine($"O preço final é {comDesconto}");

            //IMC
            var peso = 91.2;
            var altura = 1.82;
            var imc = peso / (Math.Pow(altura, 2));
            Console.WriteLine($"O imc é {imc.ToString("F2")}");

            //Modulo
            var par = 24;
            var impar = 23;
            Console.WriteLine($"{par} dividido por 2 tem resto {par % 2}");
            Console.WriteLine($"{impar} dividido por 2 tem resto {impar % 2}");
        }
    }
}
