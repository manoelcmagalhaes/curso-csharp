﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Fundamentos
{
    internal class Interpolacao
    {
        public static void Executar()
        {
            var teste = "Notebook";
            var marca = "Dell";
            var preco = 5800.00;
            Console.WriteLine("O " + teste +
                " da marca " + marca +
                " custa " + preco +
                " reais");
            Console.WriteLine("O {0} da marca {1} custa {2} reais", teste, marca, preco);
            Console.WriteLine($"A marca {marca} e boa!");
        }
    }
}
