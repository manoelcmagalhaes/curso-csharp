﻿using System;

namespace CursoCSharp.Fundamentos
{
    internal class OperadoresRelacionais
    {
        public static void Executar()
        {
            Console.WriteLine("Insira a nota");
            double.TryParse(Console.ReadLine(), out double nota);
            var notaCorte = 7.0;
            Console.WriteLine($"Nota {nota} é invalida? {nota > 10}");
            Console.WriteLine($"Nota {nota} é invalida? {nota < 0}");
            Console.WriteLine($"Nota {nota} é perfeita? {nota == 10}");
            Console.WriteLine($"Nota {nota} precisa melhorar? {nota != 10}");
            Console.WriteLine($"Passou? {nota >= notaCorte}");
            Console.WriteLine($"Recuperacao? {nota < notaCorte}");
            Console.WriteLine($"Reprovacao? {nota < 3.0}");
        }
    }
}
