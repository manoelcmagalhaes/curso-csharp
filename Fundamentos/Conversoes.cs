﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Fundamentos
{
    internal class Conversoes
    {
        public static void Executar()
        {
            var inteiro = 10;
            double quebrado = inteiro;
            Console.WriteLine(quebrado);

            double nota = 9.7;
            int notaTruncada = (int) nota;
            Console.WriteLine(notaTruncada);

            Console.WriteLine("Digite sua idade:");
            string idadeStr = Console.ReadLine();
            int idadeInt = int.Parse(idadeStr);
            Console.WriteLine($"A idade é: {idadeInt} (parse)");

            idadeInt = Convert.ToInt32(idadeStr);
            Console.WriteLine($"A idade é: {idadeInt} (convert)");

            Console.WriteLine("Digite sua idade:");
            int.TryParse(Console.ReadLine(), out idadeInt);
            Console.WriteLine($"A idade é: {idadeInt} (tryparse)");


        }
    }
}
