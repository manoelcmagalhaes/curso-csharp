﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.EstruturaDeControle
{
    internal class ForEach
    {
        public static void Executar()
        {
            string palavra = "Teste";
            string inverso = "";
            foreach (var letra in palavra)
            {
                Console.WriteLine(letra);
                inverso = letra + inverso;
                if (inverso.Length == palavra.Length)
                {
                    Console.WriteLine(inverso);
                }
            }

            var alunos = new string[] { "Manoel", "Naiana", "Mia" };
            foreach (var nome in alunos)
            {
                Console.WriteLine(nome);
            }
        }
    }
}
