﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.EstruturaDeControle
{
    internal class EstruturaIfElseIf
    {
        public static void Executar()
        {
            Console.WriteLine("Insira a nota:");
            double.TryParse(Console.ReadLine(), out double nota);
            if (nota > 9.0)
            {
                Console.WriteLine("Aprovado com boa nota!");
            }
            else if (nota > 6.0)
            {
                Console.WriteLine("Aprovado por média");
            }
            else
            {
                Console.WriteLine("Reprovado!");
            }
        }
    }
}
