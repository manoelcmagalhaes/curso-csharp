﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.EstruturaDeControle
{
    internal class EstruturaIf
    {
        public static void Executar()
        {
            var bomComportamento = false;
            string entrada;
            Console.WriteLine("Digite a nota: ");
            Double.TryParse(Console.ReadLine(), out double nota);
            Console.WriteLine("Possui bom comportamento? ");
            var comp = Console.ReadLine();
            bomComportamento = comp.Equals("S") || comp.Equals("s");
            if (nota >= 9.0 && bomComportamento)
            {
                Console.WriteLine("Quadro de honra");
            }
            else
            {
                Console.WriteLine("Pega o beco");
            }
        }
    }
}
