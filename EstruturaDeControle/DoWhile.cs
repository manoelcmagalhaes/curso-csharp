﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.EstruturaDeControle
{
    internal class DoWhile
    {
        public static void Executar()
        {
            string entrada;
            do
            {
                Console.WriteLine("Qual o seu nome?");
                Console.WriteLine($"Seja bem vindo {Console.ReadLine()}");
                Console.WriteLine("Deseja continuar? (S/N)");
                entrada = Console.ReadLine();

            } while (entrada.ToLower().Equals("s"));
        }
    }
}
