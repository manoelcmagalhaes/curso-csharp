﻿using System;
using System.IO;

namespace CursoCSharp.API
{
    internal class LeituraArquivo
    {
        public static void Executar()
        {
            var path = @"~/lendo_arquivos.csv".ParseHome();
            if (!File.Exists(path))
            {
                using (StreamWriter sw = new StreamWriter(path))
                {
                    Console.WriteLine("Arquivo criado em:");
                    Console.WriteLine(path);
                    sw.WriteLine("Produto;Preço;Qnt");
                    sw.WriteLine("Caneta Esferográfica;1,50;3");
                    sw.WriteLine("Lapis 2B;1,70;20");
                }
            }

            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    var texto = sr.ReadToEnd();
                    Console.WriteLine(texto);
                    foreach(string linha in texto.Split('\n'))
                    {
                        Console.WriteLine(linha);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
