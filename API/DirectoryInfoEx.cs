﻿using System;
using System.IO;

namespace CursoCSharp.API
{
    internal class DirectoryInfoEx
    {
        public static void Executar()
        {
            var disImagens = @"~/Pictures".ParseHome();
            var dirInfor = new DirectoryInfo(disImagens);
            if (!dirInfor.Exists)
            {
                dirInfor.Create();
            }

            Console.WriteLine("Arquivos ----------------");
            var arquivos = dirInfor.GetFiles();
            foreach (var arquivo in arquivos)
            {
                Console.WriteLine(arquivo);
            }
            Console.WriteLine("Pastas ------------------");
            var pastas = dirInfor.GetDirectories();
            foreach (var pasta in pastas)
            {
                Console.WriteLine(pasta);
            }
        }
    }
}
