﻿using System;
using System.IO;

namespace CursoCSharp.API
{
    internal class Diretorios
    {
        public static void Executar()
        {
            var novoDiretorio = @"~/ParseCSharp".ParseHome();
            var novoDiretorioDestino = @"~/ParseCSharpDestino".ParseHome();
            var diretorioProjeto = @"~/Pictures".ParseHome();

            if (Directory.Exists(novoDiretorio))
            {
                Directory.Delete(novoDiretorio, true);
            }
            if (Directory.Exists(novoDiretorioDestino))
            {
                Directory.Delete(novoDiretorioDestino, true);
            }
            Directory.CreateDirectory(novoDiretorio);
            Console.WriteLine(Directory.GetCreationTime(novoDiretorioDestino));
            Console.WriteLine("Pastas --------------------------");
            var pastas = Directory.GetDirectories(diretorioProjeto);
            foreach (var pasta in pastas)
            {
                Console.WriteLine($"Arquivos da pasta: {pasta}");
            }
            Console.WriteLine("Arquivos -------------------------");
            var arquivos = Directory.GetFiles(diretorioProjeto);
            foreach(var arquivo in arquivos)
            {
                Console.WriteLine(arquivo);
            }
            Console.WriteLine("Pastas e Arquivos ----------------");
            var pastasSub = Directory.GetDirectories(diretorioProjeto);
            foreach (var pasta in pastasSub)
            {
                Console.WriteLine($"\tArquivos da pasta: {pasta}");
                var arquivosPasta = Directory.GetFiles(pasta);
                foreach (var arquivo in arquivosPasta)
                {
                    Console.WriteLine($"\t\t{arquivo}");
                }
            }
        }
    }
}
