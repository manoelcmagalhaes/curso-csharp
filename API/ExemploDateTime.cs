﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.API
{
    internal class ExemploDateTime
    {
        public static void Executar()
        {
            var datetime = new DateTime(year: 2030, month: 2, day: 6);
            Console.WriteLine(datetime.Day);
            Console.WriteLine(datetime.Month);
            Console.WriteLine(datetime.Year);

            var hoje = DateTime.Today;
            Console.WriteLine(hoje);

            var agora = DateTime.Now;
            Console.WriteLine(agora);
            Console.WriteLine(agora.Hour);
            Console.WriteLine(agora.ToString("dd"));
            Console.WriteLine(agora.ToString("d"));
            Console.WriteLine(agora.ToString("D"));
            Console.WriteLine(agora.ToString("g"));
            Console.WriteLine(agora.ToString("G"));
            Console.WriteLine(agora.ToString("dd-MM-yyyy HH:mm"));
        }
    }
}
