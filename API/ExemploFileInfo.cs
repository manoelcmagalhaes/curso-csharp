﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.API
{
    
    internal class ExemploFileInfo
    {
        public static void ExcluirSeExistir(params string[] caminhos)
        {
            foreach (var caminho in caminhos)
            {
                FileInfo arquivo = new FileInfo(caminho);
                if (arquivo.Exists)
                {
                    arquivo.Delete();
                }
            }
        }
        public static void Executar()
        {
            var caminhoOrigem = @"~/arq_origem.txt".ParseHome();
            var caminhoDestino = @"~/arq_destino.txt".ParseHome();
            var caminhoCopia = @"~/arq_copia.txt".ParseHome();

            ExcluirSeExistir(caminhoOrigem, caminhoDestino, caminhoCopia);

            using(StreamWriter sw = File.CreateText(caminhoOrigem))
            {
                Console.WriteLine("Cria e escreve arquivos...");
                sw.WriteLine("Original");
            }


            FileInfo fi = new FileInfo(caminhoOrigem);
            Console.WriteLine(fi.Name);
            Console.WriteLine(fi.IsReadOnly);
            Console.WriteLine(fi.Extension);

            Console.WriteLine("Copiando arquivos...");
            fi.CopyTo(caminhoCopia);

            Console.WriteLine("Movendo arquivos...");
            fi.MoveTo(caminhoDestino);
        }
    }
}
