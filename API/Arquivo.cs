﻿using System;
using System.IO;

namespace CursoCSharp.API
{
    public static class ExtensaoString
    {
        public static string ParseHome(this string path)
        {
            string home = (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX) ? Environment.GetEnvironmentVariable("HOME") : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
            return path.Replace("~", home);
        }
    }
    internal class Arquivo
    {
        public static void Executar()
        {
            var path = @"~/primeiro-arquivo.txt".ParseHome();
            if (!File.Exists(path))
            {
                //Cria e escreve no arquivo
                using(StreamWriter sw = new StreamWriter(path))
                {
                    Console.WriteLine("Arquivo criado em:");
                    Console.WriteLine(path);
                    sw.WriteLine("Teste de uso de arquivo");
                    sw.WriteLine("Essa é a segunda linha");
                }
                //Escreve em um arquivo já existente
                using(StreamWriter sw = File.AppendText(path))
                {
                    Console.WriteLine("Novas linhas adicionadas ao arquivo em:");
                    Console.WriteLine(path);
                    sw.WriteLine("Teste de reescrita");
                    sw.WriteLine("Mais uma linha");
                }

            }
        }
    }
}
