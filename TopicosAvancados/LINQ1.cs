﻿using CursoCSharp.Fundamentos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CursoCSharp.TopicosAvancados
{
    public class Aluno
    {
        public string Nome;
        public int Idade;
        public double Nota;
    }
    internal class LINQ1
    {
        public static void Executar()
        {
            var alunos = new List<Aluno> {
                new Aluno(){Nome = "Manoel", Idade = 35, Nota = 8.0},
                new Aluno(){Nome = "Naiana", Idade = 36, Nota = 10.0},
                new Aluno(){Nome = "Mia", Idade = 2, Nota = 1.0},
            };

            Console.WriteLine("== Aprovados ===================");
            var aprovados = alunos.Where(a => a.Nota > 7).OrderBy(a => a.Nome);
            foreach (var aprovado in aprovados)
            {
                Console.WriteLine(aprovado.Nome);
            }

            Console.WriteLine("== Chamada =====================");
            var chamada = alunos.OrderBy(a => a.Nome).Select(a => a.Nome);
            foreach (var aluno in chamada)
            {
                Console.WriteLine($"nome: {aluno}, tipo: {chamada.GetType()}");
            }

            Console.WriteLine("== Alunos Aprovados ============");
            var alunoAprovados =
                from aluno in alunos
                where aluno.Nota > 7
                orderby aluno.Idade
                select aluno.Nome;
            foreach (var aluno in alunoAprovados)
            {
                Console.WriteLine($"nome: {aluno}");
            }
        }
    }
}
