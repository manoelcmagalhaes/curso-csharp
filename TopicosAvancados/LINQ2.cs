﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.TopicosAvancados
{
    internal class LINQ2
    {
        public static void Executar()
        {
            var alunos = new List<Aluno> {
                new Aluno(){Nome = "Manoel", Idade = 35, Nota = 8.0},
                new Aluno(){Nome = "Naiana", Idade = 36, Nota = 10.0},
                new Aluno(){Nome = "Mia", Idade = 2, Nota = 1.0},
            };

            var mia = alunos.Single(aluno => aluno.Nome.Equals("Mia"));
            Console.WriteLine($"{mia.Nome}, {mia.Nota}");

            var fulano = alunos.SingleOrDefault(aluno => aluno.Nome.Equals("Jão"));
            Console.WriteLine(fulano == null ? "Aluno inexistente": $"{fulano.Nome}, {fulano.Nota}");

            var nana = alunos.First(aluno => aluno.Nome.Equals("Naiana"));
            Console.WriteLine($"{nana.Nome}, {nana.Nota}");

            var iana = alunos.FirstOrDefault(aluno => aluno.Nome.Equals("Iana"));
            Console.WriteLine(iana == null ? "Aluno inexistente" : $"{iana.Nome}, {iana.Nota}");

            var exemploSkip = alunos.Skip(1).Take(2);
            foreach (var item in exemploSkip)
            {
                Console.WriteLine(item.Nome);
            }

            var maiorNota = alunos.Max(aluno => aluno.Nota);
            Console.WriteLine(maiorNota);

            var menorNota = alunos.Min(aluno => aluno.Nota);
            Console.WriteLine(menorNota);

            var somatorioNota = alunos.Sum(aluno => aluno.Nota);
            Console.WriteLine(somatorioNota);

            var mediaGeralDaTurma = alunos.Average(aluno => aluno.Nota);
            Console.WriteLine(mediaGeralDaTurma);

            var mediaAprovadosDaTurma = alunos.Where(aluno => aluno.Nota > 7).Average(aluno => aluno.Nota);
            Console.WriteLine(mediaAprovadosDaTurma);
        }
    }
}
