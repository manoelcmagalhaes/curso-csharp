﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.ClassesEMetodos
{
    public class CalculadoraEstatica
    {
        public static int Somar(int a, int b)
        {
            return a + b;
        }

        public static int Multiplica(int a, int b)
        {
            return a * b;
        }
    }
    internal class MetodoEstatico
    {
        public static void Executar()
        {
            var res = CalculadoraEstatica.Multiplica(2, 3);
            Console.WriteLine($"O resultado é {res}");
            Console.WriteLine($"O resultado é {CalculadoraEstatica.Somar(2, 3)}");
        }
    }
}
