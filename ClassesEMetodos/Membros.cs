﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.ClassesEMetodos
{
    internal class Membros
    {
        public static void Executar()
        {
            Pessoa teste = new Pessoa();
            Pessoa teste2 = new Pessoa();
            teste.Nome = "Manoel";
            teste2.Nome = "Nana";
            teste.Idade = 35;
            teste2.Idade = 37;
            Console.WriteLine(teste.Apresentar());

        }
    }
}
