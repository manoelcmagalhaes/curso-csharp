﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.ClassesEMetodos
{
    internal class Construtores
    {
        public static void Executar()
        {
            var carro1 = new Carro();
            carro1.Fabricante = "Ford";
            carro1.Modelo = "Courier";
            carro1.Ano = 2000;
            Console.WriteLine(carro1.ToString());
            var carro2 = new Carro("Ka", "Ford", 2010);
            Console.WriteLine(carro2.ToString());


        }
    }

    class Carro
    {
        public string Modelo;
        public string Fabricante;
        public int Ano;

        public String ToString()
        {
            return $"{Modelo}, {Fabricante}, {Ano}";
        }

        public Carro()
        {

        }

        public Carro(string modelo, string fabricante, int ano)
        {
            Modelo = modelo;
            Fabricante = fabricante;
            Ano = ano;
        }
    }
}
