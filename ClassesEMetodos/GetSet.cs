﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.ClassesEMetodos
{
    public class Moto
    {
        private string Marca;
        private string Modelo;
        private int Cilindrada;

        public Moto(string marca, string modelo, int cilindrada)
        {
            Marca = marca;
            Modelo = modelo;
            if (cilindrada > 0)
            {
                Cilindrada = cilindrada;
            }
            else
            {
                Cilindrada = cilindrada * -1;
            }
        }

        public Moto()
        {

        }

        public string GetMarca()
        {
            return Marca;
        }

        public void SetMarca(string marca)
        {
            Marca = marca;
        }

        public string GetModelo()
        {
            return Modelo;
        }

        public void SetModelo(string modelo)
        {
            Modelo = modelo;
        }

        public int GetCilindrada()
        {
            return Cilindrada;
        }

        public void SetCilindrada(int cilindrada)
        {
            if (cilindrada > 0)
            {
                Cilindrada = cilindrada;
            }
            else
            {
                Cilindrada = cilindrada * -1;
            }
        }

        public void Exibe()
        {
            Console.WriteLine($"marca: {Marca}, modelo: {Modelo}, cilindrada: {Cilindrada}");
        }
    }

    internal class GetSet
    {
        public static void Executar()
        {
            var moto1 = new Moto("Kawazaki", "Ninja", 600);
            moto1.Exibe();
            var moto2 = new Moto("KTM", "Duke", -200);
            moto2.Exibe();
        }
    }
}
