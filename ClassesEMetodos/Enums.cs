﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.ClassesEMetodos
{
    public enum Genero { Acao, Aventura, Terror, Animacao, Comedia };

    public class Filme
    {
        public string Titulo;
        public Genero GeneroDoFilme;
    }

    internal class Enums
    {
        public static void Executar()
        {
            int id = (int)Genero.Animacao;
            Console.WriteLine(id);
            var filmePraFamilia = new Filme();
            filmePraFamilia.Titulo = "Sharknado";
            filmePraFamilia.GeneroDoFilme = Genero.Comedia;
        }
    }
}
