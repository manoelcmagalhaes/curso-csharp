﻿namespace CursoCSharp.ClassesEMetodos
{
    public class CarroOpcional
    {
        double desconto = 0.1;
        string nome;
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        public double Preco { get; set; }
        public double PrecoComDesconto
        {
            get => Preco - desconto * Preco;
        }

        public CarroOpcional(string nome, double preco)
        {
            Nome = nome;
            Preco = preco;
        }

        public void Exibe()
        {
            System.Console.WriteLine($"Nome: {nome}, preco: {Preco}, preco com desconto: {PrecoComDesconto}");
        }
    }
    internal class Props
    {
        public static void Executar()
        {
            var opcional = new CarroOpcional("Ar", 3000);
            opcional.Exibe();

        }
    }
}
