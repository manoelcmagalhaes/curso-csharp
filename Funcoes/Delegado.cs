﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Funcoes
{
    delegate double Operacao(double x, double y);

    internal class Delegado
    {
        public static void Executar()
        {
            Operacao sum = (x, y) => x + y;
            Operacao min = (x, y) => x - y;
            Operacao mult = (x, y) => x * y;
            Console.WriteLine(sum(1,min(2,mult(6,7))));
        }
    }
}
