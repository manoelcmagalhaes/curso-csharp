﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Funcoes
{

    internal class Delegando
    {
        delegate double Soma(double a, double b);
        delegate void ImprimirSoma(double a, double b);

        static double MinhaSoma(double x, double y)
        {
            return x + y;
        }

        static void MeuImprimir(double a, double b)
        {
            Console.WriteLine(a + b);
        }
        public static void Executar()
        {
            Soma op1 = MinhaSoma;
            Console.WriteLine(op1(2, 3));

            ImprimirSoma op2 = MeuImprimir;
            op2(3,4);

            Func<double, double, double> op3 = MinhaSoma;
            Console.WriteLine(op3(1, 2));

            Action<double, double> op4 = MeuImprimir;
            op4(4, 9);
        }
    }
}
