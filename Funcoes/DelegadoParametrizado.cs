﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Funcoes
{
    internal class DelegadoParametrizado
    {
        public delegate int Operacao(int x, int y);

        public static int Soma(int x, int y)
        {
            return x + y;
        }

        public static string Calculadora(int x, int y)
        {
            Operacao op = Soma;
            return (op(x, y).ToString());
        }

        public static void Executar()
        {
            Console.WriteLine(Calculadora(4, 5));
        }
    }
}
