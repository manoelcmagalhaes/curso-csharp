﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Funcoes
{
    internal class Lambdas
    {
        public static void Executar()
        {
            Action algoNoConsole = () =>
            {
                Console.WriteLine("Lambda com C#");
            };

            Action<int> hoje = (dia) =>
            {
                Console.WriteLine($"hoje é: {dia}");
            };

            algoNoConsole();
            hoje(19);

            Func<int> jogarDado = () =>
            {
                return new Random().Next(1, 7);
            };
            Console.WriteLine(jogarDado());

            Func<int, string> convHex = numero =>
            {
                return numero.ToString("X");
            };
            Console.WriteLine(convHex(1234));

            Func<int, int, int, string> formataData = (dia, mes, ano) =>
            {
                return $"{dia:D2}/{mes:D2}/{ano}";
            };
            Console.WriteLine(formataData(19, 10, 2022));
        }
    }
}
