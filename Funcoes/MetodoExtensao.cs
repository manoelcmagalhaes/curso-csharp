﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Funcoes
{
    public static class ExtensoesInteiro
    {
        public static int Soma(this int num, int outro)
        {
            return num + outro;
        }

        public static int Subtracao(this int num, int outro)
        {
            return num - outro;
        }
    }

    internal class MetodoExtensao
    {
        public static void Executar()
        {
            int numero = 5;
            Console.WriteLine(numero.Soma(3));
        }
    }
}
