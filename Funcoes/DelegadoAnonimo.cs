﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Funcoes
{
    internal class DelegadoAnonimo
    {
        delegate string StringOperacao(string str);

        public static void Executar()
        {
            StringOperacao inverter = delegate (string s)
            {
                char[] array = s.ToCharArray();
                Array.Reverse(array);
                return new string(array);
            };

            Console.WriteLine(inverter("katchau!"));
        }
    }
}
