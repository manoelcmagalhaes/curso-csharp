﻿using System;
using System.Collections.Generic;

using CursoCSharp.Fundamentos;
using CursoCSharp.EstruturaDeControle;
using CursoCSharp.ClassesEMetodos;
using CursoCSharp.Colecoes;
using CursoCSharp.OO;
using CursoCSharp.Funcoes;
using CursoCSharp.API;
using CursoCSharp.TopicosAvancados;

namespace CursoCSharp {
    class Program {
        static void Main(string[] args) {
            var central = new CentralDeExercicios(new Dictionary<string, Action>() {
                //Fundamentos
                {"Primeiro Programa - Fundamentos", PrimeiroPrograma.Executar},
                {"Comentarios - Fundamentos", Comentarios.Executar },
                {"Váriaveis e Constantes - Fundamentos", VariaveisEConstantes.Executar },
                {"Inferencia - Fundamentos", Inferencia.Executar },
                {"Interpolacao - Fundamentos", Interpolacao.Executar },
                {"Notacao Ponto - Fundamentos", NotacaoPonto.Executar },
                {"Leitura de Dados - Fundamentos", LendoDados.Executar },
                {"Formatação de Numeros - Fundamentos", FormatandoNumero.Executar },
                {"Conversoes - Fundamentos", Conversoes.Executar },
                {"Operadores Aritimeticos - Fundamentos", OperadoresAritimeticos.Executar },
                {"Operadores Relacionais - Fundamentos", OperadoresRelacionais.Executar},
                {"Operadores Logicos - Fundamentos", OperadoresLogicos.Executar},
                {"Operadores de Atribuição - Fundamentos", OperadoresAtribuicao.Executar},
                {"Operadores Unários - Fundamentos", OperadoresUnarios.Executar},
                {"Operadores Ternário - Fundamentos", OperadorTernario.Executar},
                //Estruturas de controle
                {"Estrutura IF - Estrutura de Controle", EstruturaIf.Executar},
                {"Estrutura IF/ELSE - Estrutura de Controle", EstruturaIfElse.Executar},
                {"Estrutura IF/ELSE/IF - Estrutura de Controle", EstruturaIfElseIf.Executar},
                {"Estrutura Switch - Estrutura de Controle", EstruturaSwitch.Executar},
                {"Estrutura While - Estrutura de Controle", While.Executar},
                {"Estrutura DoWhile - Estrutura de Controle", DoWhile.Executar},
                {"Estrutura For - Estrutura de Controle", For.Executar},
                {"Estrutura ForEach - Estrutura de Controle", ForEach.Executar},
                //Classes e Metodos
                {"Membros - Classes e Metodos", Membros.Executar},
                {"Construtores - Classes e Metodos", Construtores.Executar},
                {"Metodos com Retorno - Classes e Metodos", MetodosComRetorno.Executar},
                {"Metodos Estatico - Classes e Metodos", MetodoEstatico.Executar},
                {"Atributos Estaticos - Classes e Metodos", AtributosEstaticos.Executar},
                {"Desafio Atributo - Classes e Metodos", DesafioAtributo.Executar},
                {"Params - Classes e Metodos", Params.Executar},
                {"Parametros Nomeados - Classes e Metodos", ParametrosNomeados.Executar},
                {"Getters and Setters - Classes e Metodos", GetSet.Executar},
                {"Props - Classes e Metodos", Props.Executar},
                {"Readonly - Classes e Metodos", Readonly.Executar},
                {"Enum - Classes e Metodos", Enums.Executar},
                {"Structs - Classes e Metodos", Structs.Executar},
                {"Parametros por Referencia - Classes e Metodos", ParamRef.Executar},
                //Colecoes
                {"Array - Colecoes", Vetores.Executar},
                {"Lista - Colecoes", Lista.Executar},
                {"ArrayList - Colecoes", ColecaoArrayList.Executar},
                {"Set - Colecoes", ColecaoSet.Executar},
                {"Fila - Colecoes", Fila.Executar},
                {"Igualdade - Colecoes", Igualdade.Executar},
                {"Dicionario - Colecoes", Dicionario.Executar},
                //OO
                {"Herança - OO", Heranca.Executar},
                {"Polimorfismo - OO", Polimorfismo.Executar},
                {"Classe Abstrata - OO", ClasseAbstrata.Executar},
                {"Interface - OO", Interface.Executar},
                {"Sealed - OO", ClasseSelada.Executar},
                //Funcoes
                {"Lambdas - Funcoes", Lambdas.Executar},
                {"Delegado - Funcoes", Delegado.Executar},
                {"Delegando - Funcoes", Delegando.Executar},
                {"Delegado Anonimo - Funcoes", DelegadoAnonimo.Executar},
                {"Delegado Parametrizado - Funcoes", DelegadoParametrizado.Executar},
                {"Metodo Extensao - Funcoes", MetodoExtensao.Executar},
                //Exceções
                {"Tratamento de Excecoes - Excecoes", Excecoes.Excecoes.Executar},
                {"Excecoes Personalizadas - Excecoes", Excecoes.ExcecoesPersonalizadas.Executar},
                //API
                {"Escrevendo - Arquivos", Arquivo.Executar},
                {"Lendo - Arquivos", LeituraArquivo.Executar},
                {"File Info - Arquivos", ExemploFileInfo.Executar},
                {"Diretorios - Arquivos", Diretorios.Executar},
                {"Info Diretorios - Arquivos", DirectoryInfoEx.Executar},
                {"Path - Arquivos", ExercicioPath.Executar},
                {"DateTime - Arquivos", ExemploDateTime.Executar},
                {"TimeSpan - Arquivos", ExemploTimeSpan.Executar},
                //LINQ
                {"Linq 1 - Topicos Avancados", LINQ1.Executar},
                {"Linq 2 - Topicos Avancados", LINQ2.Executar},
                {"Nullable - Topicos Avancados", ExemploNullable.Executar},
                {"Dynamics - Topicos Avancados", ExemploDynamic.Executar},
                {"Generics - Topicos Avancados", ExemploGenerics.Executar},
            });

            central.SelecionarEExecutar();
        }
    }
}