﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Colecoes
{
    internal class Fila
    {
        public static void Executar()
        {
            var fila = new Queue<string>();
            fila.Enqueue("Manoel");
            fila.Enqueue("Nana");
            Console.WriteLine(fila.Dequeue());
        }
    }
}
