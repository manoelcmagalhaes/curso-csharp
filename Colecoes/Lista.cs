﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Colecoes
{
    public class Produto
    {
        public string Nome { get; set; }
        public double Preco { get; set; }
        public string Tipo { get; set; }

        public Produto(string nome, string tipo, double preco)
        {
            Nome = nome;
            Preco = preco;
            Tipo = tipo;
        }

        public void Exibe()
        {
            Console.WriteLine($"nome: {Nome}, tipo: {Tipo}, preco: {Preco}");
        }

        public override bool Equals(object obj)
        {
            Produto produto = obj as Produto;
            bool mesmoNome = Nome== produto.Nome;
            bool mesmoTipo = Tipo== produto.Tipo;
            bool mesmoPreco = Preco== produto.Preco;
            return mesmoNome && mesmoTipo && mesmoPreco;
        }
    }
    internal class Lista
    {
        public static void Executar()
        {
            var livro = new Produto("Perelandra", "livro", 20.0);
            var combo = new List<Produto>
            {
                new Produto("Planeta Silecioso", "livro", 20.0),
                new Produto("As Cronicas de Narnia", "livro", 20.0),
                new Produto("Ortodoxia", "livro", 20.0),
            };
            var carrinho = new List<Produto>();
            carrinho.AddRange(combo);
            carrinho.Add(livro);
            foreach (var item in carrinho)
            {
                item.Exibe();
            }
        }
    }
}
