﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Colecoes
{
    internal class Vetores
    {
        public static void Executar()
        {
            string[] alunos = new string[5];
            for(int aluno = 0; aluno < alunos.Length; aluno++)
            {
                Console.WriteLine($"Insira o nome do aluno {aluno}:");
                alunos[aluno] = Console.ReadLine();
            }
            Console.WriteLine("Os alunos inseridos foram:");
            foreach(string aluno in alunos)
            {
                Console.WriteLine(aluno);
            }
        }
    }
}
