﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Colecoes
{
    internal class ColecaoSet
    {
        public static void Executar()
        {
            var livro = new Produto("Perelandra", "livro", 20.0);
            var combo = new HashSet<Produto>
            {
                new Produto("Planeta Silecioso", "livro", 20.0),
                new Produto("As Cronicas de Narnia", "livro", 20.0),
                new Produto("Ortodoxia", "livro", 20.0),
            };
            var carrinho = new HashSet<Produto>();
            carrinho.UnionWith(combo);
            carrinho.Add(livro);
            foreach (var item in carrinho)
            {
                item.Exibe();
            }
        }
    }
}
