﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Colecoes
{
    internal class Igualdade
    {
        public static void Executar()
        {
            var bic = new Produto("caneta", "caneta", 1.99);
            var bic2 = new Produto("caneta", "caneta", 1.99);
            Console.WriteLine(bic.Equals(bic2));
        }
    }
}
