﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.Colecoes
{
    internal class Dicionario
    {
        public static void Executar()
        {
            var filmes = new Dictionary<int, string>();
            filmes.Add(2000, "Gladiador");
            filmes.Add(1986, "Terminator 2");
            if (filmes.ContainsKey(1986))
            {
                Console.WriteLine(filmes[1986]);
            }

            filmes.TryGetValue(2000, out string saida);
            if(saida != null)
            {
                Console.WriteLine(saida);
            }
            else
            {
                Console.WriteLine("Não existe");
            }
            foreach(var filme in filmes)
            {
                Console.WriteLine($"{filme.Key}, {filme.Value}");
            }
        }
    }
}
