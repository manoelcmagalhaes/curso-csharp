﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.OO
{
    sealed class SemFilho
    {
        public double ValorFortuna()
        {
            return 1_000_000.65;
        }
    }

    //class SouFilho : SemFilho
    //{

    //}

    class Avo
    {
        public virtual bool HonrarNomeFamilia()
        {
            return true;
        }
    }

    class Pai : Avo
    {
        public sealed override bool HonrarNomeFamilia()
        {
            return true;
        }
    }

    class FilhoRebelde : Pai
    {
    }
    internal class ClasseSelada
    {
        public static void Executar()
        {
            SemFilho se = new SemFilho();
            Console.WriteLine(se.ValorFortuna());

            FilhoRebelde se2 = new FilhoRebelde();
            Console.WriteLine(se2.HonrarNomeFamilia());
        }
    }
}
