﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.OO
{
    public abstract class Celular
    {
        public abstract string Assistente();

        public string Tocar()
        {
            return "trim trim trim";
        }
    }

    public class Samsung : Celular
    {
        public override string Assistente()
        {
            return "Aqui é a Bixby";
        }
    }

    public class IPhone : Celular
    {
        public override string Assistente()
        {
            return "Aqui é a Siri";
        }
    }

    internal class ClasseAbstrata
    {
        public static void Executar()
        {
            var celulares = new List<Celular>()
            {
                new IPhone(),
                new Samsung()
            };

            foreach(var celularesItem in celulares)
            {
                Console.WriteLine(celularesItem.Assistente());
            }
        }
    }
}
