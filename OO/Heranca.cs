﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoCSharp.OO
{

    public class Carro
    {
        protected readonly int VelMax;
        int VelAtual;

        public Carro(int velMax)
        {
            VelMax = velMax;
        }

        protected int AltVel(int delta)
        {
            int NewVel = VelAtual + delta;
            if (NewVel < 0)
            {
                VelAtual = 0;
            }
            else if(NewVel > VelMax)
            {
                VelAtual = VelMax;
            }
            else
            {
                VelAtual = NewVel;
            }
            return VelAtual;
        }

        public virtual int Acelerar()
        {
            return AltVel(5);
        }

        public virtual int Frear()
        {
            return AltVel(-5);
        }


    }

    public class Uno : Carro
    {
        public Uno() : base(200)
        {

        }
    }

    public class Ferrari : Carro
    {
        public Ferrari() : base(300)
        {

        }

        public override int Acelerar()
        {
            return AltVel(25);
        }

        public new int Frear()
        {
            return AltVel(-25);
        }
    }

    public class Veyron : Carro
    {
        public Veyron() : base(500)
        {

        }

        public override int Acelerar()
        {
            return AltVel(75);
        }

        public override int Frear()
        {
            return AltVel(-100);
        }
    }

    internal class Heranca
    {
        public static void Executar()
        {
            Console.WriteLine("Uno...");
            Uno carro1 = new Uno();
            Console.WriteLine(carro1.Acelerar());
            Console.WriteLine(carro1.Acelerar());
            Console.WriteLine(carro1.Acelerar());
            Console.WriteLine(carro1.Frear());
            Console.WriteLine(carro1.Frear());
            Console.WriteLine(carro1.Frear());
            Console.WriteLine(carro1.Frear());

            Console.WriteLine("Ferrari...");
            Ferrari carro2 = new Ferrari();
            Console.WriteLine(carro2.Acelerar());
            Console.WriteLine(carro2.Acelerar());
            Console.WriteLine(carro2.Acelerar());
            Console.WriteLine(carro2.Frear());
            Console.WriteLine(carro2.Frear());
            Console.WriteLine(carro2.Frear());
            Console.WriteLine(carro2.Frear());

            Console.WriteLine("Veyron...");
            Carro carro3 = new Veyron();
            Console.WriteLine(carro3.Acelerar());
            Console.WriteLine(carro3.Acelerar());
            Console.WriteLine(carro3.Acelerar());
            Console.WriteLine(carro3.Frear());
            Console.WriteLine(carro3.Frear());
            Console.WriteLine(carro3.Frear());
            Console.WriteLine(carro3.Frear());
        }
    }
}
